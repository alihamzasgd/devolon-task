# Task Details
The task was to implement Rest-API for the electric vehicle charging station management system. RESTful API and simple user interface to consume the API are part of this submission.

# Implementation Details

## Development Technology Stack
* PHP - CodeIgniter
* RESTful Web Service

### RESTful API
### API Features
Rest-API has been implemented in CodeIgniter that supports following operation:

* CRUD operation for companies and station

* Implementation of companies in way that a company can own more than one child companies and has access to their stations as well as its own stations.

* Implementation of the end-point to get all stations Within the radius of n kilometers from a point (latitude, longitude) ordered by distance. Including all the children stations in the tree, for the given company.

RESTful API standards are followed to make sure efficient API performance. All of the End-points support JSON responses. Requests are authenticated through basic authentication and additionally through API Key Authentication to avoid fake requests.

### API Scalability

Scalability of the API has been kept in mind while its development. Following are the aspect that make this API scalable.

All of the end-points of the API are independent of each other as no common session or similar thing is used which means that they can be installed on different servers. Only thing needed on each server would be CodeIgniter environment.

Pagination support is given for all of the end-point that fetch data. So clients can request data as per their need which makes sure that API will work even when the data set is huge.

As most of the operation that are done through the API are dependent of each other, so no caching is used. For example, end-point for company creation expects the company to be created immediately as all of the companies’ retrieval requests will be expecting that new company in the companies list. Type of operations that could be done through API and nature of data does not allow to use any caching or queue mechanism which could have made the API much faster and readily available to entertain more requests.

Talking about hardware scalability, if the API is hosted on server like apache they will automatically use all available cores of the CPU making sure that none of the available resources is free. As PHP is single threaded language, so it cannot be done programmatically.

Haversine formula has been used to get stations in the specified radius of a given Geo Location (latitude, longitude).

.htaccess file is included to prettify API end-point URLs.

## Database

Database schema was created as suggested in the task for companies and stations tables. Two other tables for API logs and API keys were created that are used in advanced authentication.

InnoDB engine is used for all tables to support relations among the tables and avoid table level locking while DB intensive operations.

Document based databases like MongoDB were among the DB options for the API but avoided as it would have taken much long to understand their implantation and way of working.

## Web Interface
Web application was built using Bootstrap, JQuery, JavaScript and some custom styling.

Web Interface source code can be cloned from the link below:

1. https://bitbucket.org/alihamzasgd/devolon-task-interface/src/master/

# Online Resources Used:
1.	https://en.wikipedia.org/wiki/Haversine_formula

# Server Requirements
PHP version 5.6 or newer is recommended. It should work on 5.3.7 as well, but we strongly advise you NOT to run such old versions of PHP, because of potential security and performance issues, as well as missing features.

### Author
Ali Hamza
