<?php
/**
 * Created by PhpStorm.
 * User: alihamza
 * Date: 3/09/2020
 * Time: 12:40 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Station_model extends CI_Model

{

    /**
     * @param int $id
     * @return mixed
     */
    public function read($id = -1, $page, $records_per_page){

        $offset = 0;
        $total_records = 0;
        $total_pages = 0;
        $pagination_clause = "";
        $pagination = array();
        if($page != -1){
            if($records_per_page == -1 ){
                $records_per_page = DEFAUTL_PAGE_LIMIT;
            }
            $offset = ($page * $records_per_page) - $records_per_page ;
            $pagination_clause = " LIMIT $offset, $records_per_page";
            $pagination["page"] = (int)$page;
            $pagination["perPage"]= (int)$records_per_page;
        }


        if($id > 0){
            $sqlTotal = "SELECT s.*,COALESCE(comp.name, '') as parent_company_name
                    FROM station s
                    LEFT JOIN companies comp on s.company_id = comp.id
                    where s.id = $id";
            $sql = $sqlTotal.$pagination_clause;
            $data["data"] = $this->db->query($sql)->row();
        }else{
            $sqlTotal = "SELECT s.*,COALESCE(comp.name, '') as parent_company_name
                    FROM station s
                    LEFT JOIN companies comp on s.company_id = comp.id";
            $sql = $sqlTotal.$pagination_clause;
            $data["data"] = $this->db->query($sql)->result();
        }

        if($page != -1){
            $total_records = $this->db->query($sqlTotal)->num_rows();
            if($total_records % $records_per_page == 0){
                $total_pages = $total_records / $records_per_page;
            }else {
                $total_pages = floor($total_records / $records_per_page) + 1;
            }
            $pagination["totalPages"] = $total_pages;
        }
        $data["pagination"] = $pagination;
        return $data;
    }


    /**
     * @param int $company_id
     * @return mixed
     */
    public function readCompanyStations($company_id = -1){

        if($company_id > 0){
            $data = $this->db->get_where("station", ['company_id' => $company_id])->row_array();
        }else{
            $data = $this->db->get("station")->result();
        }
        return $data;
    }


    /**
     * Function to get stations in c certain radius for a company ID and its all nested children.
     * Distance is calculated using Haversine formula
     * @param int $company_id
     * @return mixed
     */
    public function readCompanyStationsInRadius($company_id = -1, $latitude, $longitude, $radius, $page, $records_per_page){

        $offset = 0;
        $total_records = 0;
        $total_pages = 0;
        $pagination_clause = "";
        $pagination = array();
        if($page != -1){
            if($records_per_page == -1 ){
                $records_per_page = DEFAUTL_PAGE_LIMIT;
            }
            $offset = ($page * $records_per_page) - $records_per_page ;
            $pagination_clause = " LIMIT $offset, $records_per_page";
            $pagination["page"] = (int)$page;
            $pagination["perPage"]= (int)$records_per_page;
        }

        $data = array();
        if($company_id > 0){
            $sqlTotal  = "SELECT
                            st.id, st.name, st.latitude, st.longitude,
                            ROUND(
                                (
                                    6371 * acos(
                                        cos(radians($latitude)) * cos(radians(latitude)) * cos(
                                            radians(longitude) - radians($longitude)
                                        ) + sin(radians($latitude)) * sin(radians(latitude))
                                    )
                                ) 
                            , 2 ) AS distance
                        FROM
                            Station st
                        WHERE 
                            st.company_id IN ($company_id)
                        HAVING
                            distance < $radius
                        ORDER BY
                            distance
                        ";
            $sql = $sqlTotal.$pagination_clause;
            $data["data"] = $this->db->query($sql)->result();
        }

        if($page != -1){
            $total_records = $this->db->query($sqlTotal)->num_rows();
            if($total_records % $records_per_page == 0){
                $total_pages = $total_records / $records_per_page;
            }else {
                $total_pages = floor($total_records / $records_per_page) + 1;
            }
            $pagination["totalPages"] = $total_pages;
        }
        $data["pagination"] = $pagination;

        return $data;
    }


    /**
     * @param $data
     * @return bool
     */
    public function insert($data)
    {
        $this->name = $data['name'];
        $this->latitude = $data['latitude'];
        $this->longitude = $data['longitude'];
        $this->company_id = $data['company_id'];

        if ($this->db->insert('station', $this)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id,$data){
        $this->name    = $data['name'];
        $this->latitude = $data['latitude'];
        $this->longitude = $data['longitude'];
        $this->company_id = $data['company_id'];
        $result = $this->db->update('station',$this,array('id' => $id));

        if($result)
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->db->delete('station', array('id'=>$id));
        if($result)
        {
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    /**
     * Function to delete all station directly under a company
     * @param $company_id
     * @return bool
     */
    public function deleteCompanyStation($company_id)
    {
        $result = $this->db->delete('station', array('company_id'=>$company_id));
        if($result)
        {
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }


}