<?php
/**
 * Created by PhpStorm.
 * User: alihamza
 * Date: 3/09/2020
 * Time: 12:40 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model

{

    /**
     * @param int $id
     * @return mixed
     */
    public function read($id = -1, $page, $records_per_page){

        $offset = 0;
        $total_records = 0;
        $total_pages = 0;
        $pagination_clause = "";
        $pagination = array();
        if($page != -1){
            if($records_per_page == -1 ){
                $records_per_page = DEFAUTL_PAGE_LIMIT;
            }
            $offset = ($page * $records_per_page) - $records_per_page ;
            $pagination_clause = " LIMIT $offset, $records_per_page";
            $pagination["page"] = (int)$page;
            $pagination["perPage"]= (int)$records_per_page;
        }

        if($id > 0){
            $sqlTotal = "SELECT c.*,COALESCE(comp.name, '') as parent_company_name
                    FROM companies c
                    LEFT JOIN companies comp on c.parent_company_id = comp.id
                    where c.id = $id";
            $sql = $sqlTotal.$pagination_clause;
            $data["data"] = $this->db->query($sql)->row();
        }else{
            $sqlTotal = "SELECT c.*,COALESCE(comp.name, '') as parent_company_name
                    FROM companies c
                    LEFT JOIN companies comp on c.parent_company_id = comp.id
                    ";
            $sql = $sqlTotal.$pagination_clause;
            $data["data"] = $this->db->query($sql)->result();
        }

        if($page != -1){
            $total_records = $this->db->query($sqlTotal)->num_rows();
            if($total_records % $records_per_page == 0){
                $total_pages = $total_records / $records_per_page;
            }else {
                $total_pages = floor($total_records / $records_per_page) + 1;
            }
            $pagination["totalPages"] = $total_pages;
        }
        $data["pagination"] = $pagination;
        return $data;
    }


    /**
     * Function to get all children of a company
     * @param int $company_id
     * @return mixed
     */
    public function getCompanyChildren($company_id = -1){

        $data = array();
        if($company_id > 0){
            $query  = "SELECT GROUP_CONCAT(lv SEPARATOR ',') as children FROM (
                        SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM Companies
                        WHERE FIND_IN_SET(parent_company_id, @pv)) AS lv FROM Companies
                        JOIN
                        (SELECT @pv:=$company_id ) tmp
                        UNION select $company_id from companies
                        ) a;
                        ";

            $result = $this->db->query($query);
            if($result->num_rows() > 0 ){
                $childrenCompaniesCommaSeparated = $result->row()->children;
                $data = explode(',',$childrenCompaniesCommaSeparated);
            }
        }
        return $data;
    }


    /**
     * @param $data
     * @return bool
     */
    public function insert($data)
    {
        $this->name = $data['name'];
        if(isset($data['parent_company_id']) && $data['parent_company_id'] != null) {
            $this->parent_company_id = $data['parent_company_id'];
        }

        if ($this->db->insert("companies", $this)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id,$data){
        $this->name = $data['name'];
        $this->parent_company_id = $data['parent_company_id'];
        $result = $this->db->update("companies",$this,array('id' => $id));

        if($result)
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete company ID from its children
     * @param $id
     * @return bool
     */
    public function updateChildrenCompanies($id){
        $this->parent_company_id = null;
        $result = $this->db->update("companies",$this,array('parent_company_id' => $id));

        if($result)
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->db->delete("companies", array('id'=>$id));
        if($result)
        {
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }


}