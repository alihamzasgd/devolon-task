<?php
/**
 * Created by PhpStorm.
 * User: alihamza
 * Date: 3/09/2020
 * Time: 2:43 PM
 */

/**
 * Function for uniform response for the whole API
 * @param $status
 * @param $message
 * @param array $data
 * @return array
 */
function formatResponse($status, $message, $data = array(), $pagination = array())
{
    if($data == null){
        $data = array();
    }
    $response = array();
    $response["status"] = $status;
    $response["message"] = $message;
    $response["data"] = $data;
    $response["pagination"] = $pagination;
    return $response;
}

/**
 * Function for uniform response for the whole API
 * @param $status
 * @param $message
 * @param array $data
 * @return array
 */
function formatErrorResponse($status, $message, $data = array())
{
    if($data == null){
        $data = array();
    }
    $response = array();
    $response["status"] = $status;
    $response["message"] = $message;
    $response["errors"] = $data;
    return $response;
}
