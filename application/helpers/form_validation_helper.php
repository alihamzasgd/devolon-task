<?php
/**
 * Created by PhpStorm.
 * User: alihamza
 * Date: 3/11/2020
 * Time: 4:25 PM
 */


function getValidationRules($entity)
{
    $rules = array();
    if($entity == "station"){
        $station_validation_rules = [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Name is required.'
                ],
            ],
            [
                'field' => 'latitude',
                'label' => 'latitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must provide latitude.'
                ],
            ],
            [
                'field' => 'longitude',
                'label' => 'longitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must provide longitude.'
                ],
            ],
            [
                'field' => 'company_id',
                'label' => 'company_id',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must Specify Owner Company'
                ],
            ]
        ];

        $rules = $station_validation_rules;
    }

    if($entity == "company"){
        $company_validation_rules = [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Name is required.'
                ],
            ]
        ];
        $rules = $company_validation_rules;
    }

    if($entity == "companyStationsInRadius"){
        $company_station_validation_rules = [
            [
                'field' => 'company_id',
                'label' => 'company_id',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Company ID is required.'
                ],
            ],
            [
                'field' => 'latitude',
                'label' => 'latitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must provide latitude.'
                ],
            ],
            [
                'field' => 'longitude',
                'label' => 'longitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must provide longitude.'
                ],
            ],
            [
                'field' => 'radius',
                'label' => 'radius',
                'rules' => 'required',
                'errors' => [
                    'required' => 'You must specify Redius'
                ],
            ]
        ];

        $rules = $company_station_validation_rules;
    }

    return $rules;
}