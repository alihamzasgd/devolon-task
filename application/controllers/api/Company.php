<?php
/**
 * Created by PhpStorm.
 * User: alihamza
 * Date: 3/09/2020
 * Time: 11:14 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Company extends REST_Controller {

    public function __construct() {
        
        parent::__construct();
        
        ### BASIC AUTHENTICATION OF API KEY ###
        if(!isset($_SERVER["HTTP_X_API_KEY"])){
            $response_status = REST_Controller::HTTP_UNAUTHORIZED;
                $response_message = "No API Key Provided.";
                $response = formatErrorResponse($response_status, $response_message);
                echo json_encode($response); 
                exit();
        }else{
            if($_SERVER["HTTP_X_API_KEY"] != API_KEY){
                $response_status = REST_Controller::HTTP_UNAUTHORIZED;
                $response_message = "Invalid API Key.";
                $response = formatErrorResponse($response_status, $response_message);
                echo json_encode($response); 
                exit();
            }
        }

        $this->load->model('company_model');
    }

    /**
     * Function to get details of all or specified company
     * @return Response
     */
    public function index_get($id = -1)
    {
        $response_status = REST_Controller::HTTP_OK;
        $response_message = "";
        $page = -1;
        $records_per_page = -1;

        if(isset($_GET["page"]) && isset($_GET["page"]) > 0){
            $page = $_GET["page"];
        }

        if(isset($_GET["perPage"]) && isset($_GET["perPage"]) > 0){
            $records_per_page = $_GET["perPage"];
        }

        $results = $this->company_model->read($id, $page, $records_per_page);
        $data = $results["data"];
        $pagination = $results["pagination"];
        if(count($data) <= 0){
            $response_message = "No Data Found";
            $response_status = REST_Controller::HTTP_NOT_FOUND;
        }
        $response = formatResponse($response_status, $response_message,$data,$pagination);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    /**
     * Function to create a new company
     * @return Response
     */
    public function index_post()
    {
        $response_status = 0;
        $response_message = "";
        $inputData = json_decode(file_get_contents("php://input"), true);
        $inputData["name"] = str_replace("+"," ",$inputData["name"]);

        $this->form_validation->set_data($inputData);
        $validation_rules = getValidationRules("company");
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run()==FALSE)
        {
            $errors = $this->form_validation->error_array();
            $response_status = REST_Controller::HTTP_BAD_REQUEST;
            $response_message = 'Company was not created.';

            $response = formatErrorResponse($response_status,$response_message,$errors);
            $this->response($response, $response_status);
        }
        else
        {
            $data = array('name' => $inputData['name'],
                'parent_company_id' => $inputData['parent_company_id']
            );
            $status = $this->company_model->insert($data);

            if ($status === true) {
                $response_status = REST_Controller::HTTP_CREATED;
                $response_message = 'Company created successfully.';
            } else {
                $response_status = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $response_message = 'Company was not created.';
            }

            $response = formatResponse($response_status, $response_message);
            $this->response($response, $response_status);
        }

    }

    /**
     * Function to update Details of a company
     * @return Response
     */
    public function index_put($id)
    {
        $response_status = 0;
        $response_message = "";

        $inputData = json_decode(file_get_contents("php://input"), true);
        $inputData["name"] = str_replace("+"," ",$inputData["name"]);

        $this->form_validation->set_data($inputData);
        $validation_rules = getValidationRules("company");
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run()==FALSE)
        {
            $errors = $this->form_validation->error_array();
            $response_status = REST_Controller::HTTP_BAD_REQUEST;
            $response_message = 'Company was not updated.';

            $response = formatErrorResponse($response_status,$response_message,$errors);
            $this->response($response, $response_status);
        }
        else
        {
            $data = array('name' => $inputData['name']);
            if(isset($inputData['parent_company_id']))
            {
                $data['parent_company_id'] = $inputData['parent_company_id'];
            }else{
                $data['parent_company_id'] = null;
            }

            $status = $this->company_model->update($id, $data);
            if ($status === true) {
                $response_status = REST_Controller::HTTP_OK;
                $response_message = 'Company updated successfully.';
            } else {
                $response_status = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $response_message = 'Company was not updated.';
            }

            $response = formatResponse($response_status, $response_message);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    /**
     * delete a certain company.
     * @return Response
     */
    public function index_delete($id)
    {

        $this->load->model('station_model');

        $response_status = 0;
        $response_message = "";

        $status = $this->company_model->delete($id);

        ### delete stations of the company
        $this->station_model->deleteCompanyStation($id);

        ### Remove parent company from direct children of the deleted companies
        $this->company_model->updateChildrenCompanies($id);

        if($status > 0) {
            $response_status = REST_Controller::HTTP_OK;
            $response_message = 'Company deleted successfully.';
        }else {
            $response_status = REST_Controller::HTTP_NOT_FOUND;
            $response_message = 'No matching record found.';
        }
        $response = formatResponse($response_status,$response_message);
        $this->response($response, REST_Controller::HTTP_OK);
    }


    /**
     * Function to get all companies of a single company
     * @return Response
     */
    public function companyChildren_get($company_id = -1)
    {
        $response_status = REST_Controller::HTTP_OK;
        $response_message = "";

        $data = $this->company_model->getCompanyChildren($company_id);
        if(count($data) <= 0){
            $response_message = "No Data Found";
            $response_status = REST_Controller::HTTP_NOT_FOUND;
        }
        $response = formatResponse($response_status, $response_message,$data);
        $this->response($response, REST_Controller::HTTP_OK);
    }
    
}